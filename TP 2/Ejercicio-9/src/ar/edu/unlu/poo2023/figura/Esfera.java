package ar.edu.unlu.poo2023.figura;

public class Esfera extends FiguraTresDimensiones{

    private double radio;
    private final double PI = 3.14159265359;

    public Esfera(double radio){
        this.radio = radio;
    }

    public double getRadio(){
        return this.radio;
    }

    public double getPI(){
        return this.PI;
    }

    public double getArea(){
        return 4 * PI * Math.pow(radio,2);
    }

    public double getVolumen(){
        return (4/3) * PI * Math.pow(radio,3);
    }
}
