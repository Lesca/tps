package ar.edu.unlu.poo2023.figura;

public class TetraedroRegular extends FiguraTresDimensiones{
    private double arista;

    public TetraedroRegular(double arista){
        this.arista = arista;
    }

    public double getArista(){
        return this.arista;
    }

    public double getArea() {
        return Math.pow(arista, 3) * Math.sqrt(3);
    }

    public double getVolumen(){
        return Math.pow(arista, 3) * (Math.sqrt(2)/12);
    }
}
