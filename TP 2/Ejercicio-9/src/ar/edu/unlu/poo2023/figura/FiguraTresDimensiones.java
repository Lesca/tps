package ar.edu.unlu.poo2023.figura;

public abstract class FiguraTresDimensiones extends FiguraGeometrica {

    public abstract double getVolumen();
}
