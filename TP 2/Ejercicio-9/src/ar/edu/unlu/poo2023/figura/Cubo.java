package ar.edu.unlu.poo2023.figura;

public class Cubo extends FiguraTresDimensiones{
    private double arista;

    public Cubo(double arista){
        this.arista = arista;
    }

    public double getArista() {
        return arista;
    }

    public double getArea(){
        return 6 * Math.pow(arista, 2);
    }

    public double getVolumen(){
        return Math.pow(arista, 3);
    }
}
