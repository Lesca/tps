package ar.edu.unlu.poo2023.figura;

public class Triangulo extends FiguraDosDimensiones{
    private double base;
    private double altura;

    public Triangulo(double base, double altura){
        this.base = base;
        this.altura = altura;
    }

    public double getArea(){
        return (base * altura) / 2;
    }
}
