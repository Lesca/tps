package ar.edu.unlu.poo2023.figura;

public abstract class FiguraGeometrica {
    public abstract double getArea();
}
