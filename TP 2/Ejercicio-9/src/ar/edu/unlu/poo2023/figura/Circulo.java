package ar.edu.unlu.poo2023.figura;

public class Circulo extends FiguraDosDimensiones{
    private final double PI = 3.14159265359;
    private double radio;

    public Circulo(double radio){
        this.radio = radio;
    }

    public double getPI() {
        return this.PI;
    }

    public double getRadio() {
        return radio;
    }

    public double getArea(){
        return PI * Math.pow(radio, 2);
    }
}
