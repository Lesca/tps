import ar.edu.unlu.poo2023.figura.*;

import java.util.ArrayList;
import java.util.List;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        List<FiguraGeometrica> figuras2d = new ArrayList<>();
        List<FiguraTresDimensiones> figuras3d = new ArrayList<>();
        FiguraGeometrica circulo = new Circulo(1);
        FiguraGeometrica rectangulo = new Rectangulo(1,2);
        FiguraGeometrica cuadrado = new Cuadrado(1);
        FiguraGeometrica triangulo = new Triangulo(1,2);
        FiguraGeometrica esfera = new Esfera(1);
        FiguraGeometrica tetraedro = new TetraedroRegular(1);
        figuras2d.add(circulo);
        figuras2d.add(rectangulo);
        figuras2d.add(cuadrado);
        figuras2d.add(triangulo);
        figuras2d.add(esfera);
        figuras2d.add(tetraedro);
        FiguraTresDimensiones cubo = new Cubo(1);
        FiguraTresDimensiones paralelepipedo = new Paralelepipedo(3,2,3);
        figuras3d.add(cubo);
        figuras3d.add(paralelepipedo);

        for (FiguraGeometrica f : figuras2d){
            System.out.println("Area de "+f.getClass().getSimpleName()+": "+f.getArea());
        }

        for (FiguraTresDimensiones f: figuras3d){
            System.out.println("Volumen de "+f.getClass().getSimpleName()+": "+f.getArea());
        }
    }
}