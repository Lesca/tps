package modelo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ClubDeportivoSanCarlos{
    private List<Actividad> listaDeActividades;
    private List<Socio> listaDeSocios;
    private double montoSubscripcionBasica;
    private double montoSubscripcionIntermedia;
    private double montoSubscripcionDestacada;

    public ClubDeportivoSanCarlos(double montoSubscripcionBasica, double montoSubscripcionIntermedia, double montoSubscripcionDestacada){
        this.listaDeActividades = new ArrayList<Actividad>();
        this.listaDeSocios = new ArrayList<Socio>();
        setMontoSubscripcionBasica(montoSubscripcionBasica);
        setMontoSubscripcionIntermedia(montoSubscripcionIntermedia);
        setMontoSubscripcionDestacada(montoSubscripcionDestacada);
    }

    public double getMontoSubscripcionBasica() {
        return montoSubscripcionBasica;
    }

    public void setMontoSubscripcionBasica(double montoSubscripcionBasica) {
        this.montoSubscripcionBasica = montoSubscripcionBasica;
    }

    public double getMontoSubscripcionIntermedia() {
        return montoSubscripcionIntermedia;
    }

    public void setMontoSubscripcionIntermedia(double montoSubscripcionIntermedia) {
        this.montoSubscripcionIntermedia = montoSubscripcionIntermedia;
    }

    public double getMontoSubscripcionDestacada() {
        return montoSubscripcionDestacada;
    }

    public void setMontoSubscripcionDestacada(double montoSubscripcionDestacada) {
        this.montoSubscripcionDestacada = montoSubscripcionDestacada;
    }

    public Socio buscarSocio(String dni){
        boolean encontre = false;
        int i = 0;
        Socio s = null;
        while (!encontre && i<=listaDeSocios.size()-1){
            s = listaDeSocios.get(i);
            if (s.getDni().equals(dni)){
                encontre = true;
            } else {
                i++;
            }
        }
        if (!encontre){
            s = null;
        }
        return s;
    }

    public Actividad buscarActividad(String descripcion){
        boolean encontre = false;
        int i = 0;
        Actividad actividad = null;
        while (!encontre && i<= listaDeActividades.size()-1){
            actividad = listaDeActividades.get(i);
            if (actividad.getDescripcion().equals(descripcion)){
                encontre = true;
            } else {
                i++;
            }
        }
        if (!encontre){
            actividad = null;
        }
        return actividad;
    }

    public void agregarActividad(Actividad actividad){
        listaDeActividades.add(actividad);
    }

    public boolean agregarSocio(String dni, String nombre, String direccion, String mail,
                                TipoDeSubscripcion tipoDeSubscripcion){
        boolean resultado;
        if (buscarSocio(dni) == null){
            resultado = true;
            Socio socio = new Socio(dni, nombre, direccion, mail, tipoDeSubscripcion, calcularMontoCuota(tipoDeSubscripcion));
            listaDeSocios.add(socio);
        } else {
            resultado = false;
        }
        return resultado;
    }

    public boolean agregarActividad(String descripcion, TipoDeSubscripcion tipoDeSubscripcionRequerida){
        boolean resultado;
        if(buscarActividad(descripcion) == null){
            resultado = true;
            Actividad actividad = new Actividad(descripcion, tipoDeSubscripcionRequerida);
            listaDeActividades.add(actividad);
        } else {
            resultado = false;
        }
        return resultado;
    }

    public boolean registrarPago(String dniDeSocio){
        boolean resultado;
        Socio socio = buscarSocio(dniDeSocio);
        if (socio == null){
            resultado = false;
        } else {
            resultado = true;
            socio.pagarCuota();
        }
        return resultado;
    }

    public boolean comprobarAccesoActividad(Socio socio, Actividad actividad){
        TipoDeSubscripcion tipoSocio = socio.getTipoDeSubscripcion();
        TipoDeSubscripcion tipoRequerido = actividad.getTipoDeSubscripcionRequerida();
        return tipoSocio.compareTo(tipoRequerido) >= 0;
    }

    public String reporteMensualDeNuevosSocios(){
        String listado = "";
        for(Socio s: listaDeSocios){
            if (s.getFechaDeInscripcion().getMonth() == LocalDate.now().getMonth()){
                listado += "Socio: " + s.getNombre() + "\n";
            }
        }
        return listado;
    }

    private double calcularMontoCuota(TipoDeSubscripcion tipoDeSubscripcion) {
        switch (tipoDeSubscripcion){
            case BASICA:
                return montoSubscripcionBasica;
            case INTERMEDIA:
                return montoSubscripcionIntermedia;
            case DESTACADA:
                return montoSubscripcionDestacada;
            default:
                return 0.0;
        }
    }
}
