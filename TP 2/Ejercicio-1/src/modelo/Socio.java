package modelo;

import java.time.LocalDate;

public class Socio{
    private String dni;
    private String nombre;
    private String direccion;
    private String correoElectronico;
    private TipoDeSubscripcion tipoDeSubscripcion;
    private LocalDate fechaDeInscripcion;
    private Pago pago;
    private boolean coutaPaga;

    public Socio(String dni, String nombre, String direccion, String correoElectronico, TipoDeSubscripcion tipoDeSubscripcion,
                 double montoMensual){
        setDni(dni);
        setDireccion(direccion);
        setNombre(nombre);
        setCorreoElectronico(correoElectronico);
        setTipoDeSubscripcion(tipoDeSubscripcion);
        this.fechaDeInscripcion = LocalDate.now();
        pago = new Pago(montoMensual);
    }

    public String getDni(){
        return this.dni;
    }

    public void setDni(String dni){
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public LocalDate getFechaDeInscripcion(){
        return this.fechaDeInscripcion;
    }

    public boolean isCoutaPaga() {
        return coutaPaga;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public void setTipoDeSubscripcion(TipoDeSubscripcion tipoDeSubscripcion){
        this.tipoDeSubscripcion = tipoDeSubscripcion;
    }

    public TipoDeSubscripcion getTipoDeSubscripcion(){
        return this.tipoDeSubscripcion;
    }

    public boolean registrarPago(double monto){
        boolean resultadoDeOperacion = false;
        if (isCoutaPaga() && monto == pago.getMontoMensual()){
            //*-
        }
    }
}
