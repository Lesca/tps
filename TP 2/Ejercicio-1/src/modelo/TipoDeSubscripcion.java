package modelo;

public enum TipoDeSubscripcion {
    BASICA, INTERMEDIA, DESTACADA;
}
