package modelo;

public class Actividad {
    private String descripcion;
    private TipoDeSubscripcion tipoDeSubscripcionRequerida;

    public Actividad(String descripcion, TipoDeSubscripcion tipoDeSubscripcionRequerida){
        this.descripcion = descripcion;
        this.tipoDeSubscripcionRequerida = tipoDeSubscripcionRequerida;
    }

    public String getDescripcion(){
        return this.descripcion;
    }

    public TipoDeSubscripcion getTipoDeSubscripcionRequerida(){
        return this.tipoDeSubscripcionRequerida;
    }
}
