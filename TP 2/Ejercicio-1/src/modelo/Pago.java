package modelo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Pago {
    private List<LocalDate> historialDePagos;
    private double montoMensual;

    public Pago(double montoMensual){
        historialDePagos = new ArrayList<>();
        this.montoMensual = montoMensual;
    }

    public double getMontoMensual(){
        return this.montoMensual;
    }

    public LocalDate getFechaDePago(int mes, int ano){
        LocalDate fecha = null;
        int i = 0;
        boolean encontre = false;
        while (!encontre && i <= historialDePagos.size()-1){
            fecha = historialDePagos.get(i);
            if (fecha.getMonthValue()==mes && fecha.getYear()==ano){
                encontre = true;
            } else {
                i++;
            }
        }
        return fecha;
    }

}
