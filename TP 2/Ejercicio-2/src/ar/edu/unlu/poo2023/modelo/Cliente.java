package ar.edu.unlu.poo2023.modelo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Cliente {
    private String dni;
    private String nombre;
    private String mail;
    private String telefono;
    private List<Compra> compras;

    public Cliente(String dni, String nombre, String mail, String telefono) {
        compras = new ArrayList<>();
        this.dni = dni;
        this.nombre = nombre;
        this.mail = mail;
        this.telefono = telefono;
    }

    public String getDni() {
        return dni;
    }

    public String getNombre() {
        return nombre;
    }

    public String getMail() {
        return mail;
    }

    public String getTelefono() {
        return telefono;
    }

    public Compra buscarCompraPorFecha(LocalDate fecha){
        boolean encontre = false;
        int i = 0;
        Compra compra = null;
        while (!encontre && i <= compras.size()-1){
            compra = compras.get(i);
            if (compra.getClass().equals(fecha)){
                encontre = true;
            } else {
                i++;
            }
        }
        if (!encontre){
            compra = null;
        }
        return compra;
    }
}
