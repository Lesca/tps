package ar.edu.unlu.poo2023.modelo;

import java.util.ArrayList;
import java.util.List;

public class AgenciaDeTurismo {
    private List<Destino> destinos;
    private List<PaqueteTuristico> paquetesTuristicos;
    private List<Venta> ventas;

    public AgenciaDeTurismo(){
        destinos = new ArrayList<>();
        paquetesTuristicos = new ArrayList<>();
        ventas = new ArrayList<>();
    }

    public boolean agregarDestino(Destino destino){
        destinos.add(destino);
        // Logica de control de que no se repita el destino, no llego a complemtarlo
        return true;
    }

    public void registrarVenta(Cliente cliente, PaqueteTuristico paqueteTuristico){
        Venta venta = new Venta(cliente, paqueteTuristico);
        ventas.add(venta);
    }

    public Destino buscarDestino(String nombreDestino){
        Destino destino = null;
        boolean encontre = false;
        int i = 0;
        while (!encontre && i <= destinos.size() -1){
            destino = destinos.get(i);
            if (destino.getNombreDeDestino().equals(nombreDestino)){
                encontre = true;
            }else {
                i++;
            }
        }
        if (!encontre){
            destino = null;
        }
        return destino;
    }

    public PaqueteTuristico armarPaqueteTuristico(String descripcion, Destino destino, String razonSocialTransporte, String razonSocialHospedaje,
                                                  String razonSocialExcursion){
        PaqueteTuristico paqueteTuristico = destino.armarPaqueteTuristico(descripcion,razonSocialTransporte, razonSocialHospedaje, razonSocialExcursion);
        paquetesTuristicos.add(paqueteTuristico);
        return paqueteTuristico;
    }

    public void RegistrarVenta(PaqueteTuristico paqueteTuristico){

    }
}
