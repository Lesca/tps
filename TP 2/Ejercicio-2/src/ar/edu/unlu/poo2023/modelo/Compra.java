package ar.edu.unlu.poo2023.modelo;

import java.time.LocalDate;

public class Compra {
    private LocalDate fechaDeVenta;
    private double monto;
    private PaqueteTuristico paqueteTuristico;

    public Compra(PaqueteTuristico paqueteTuristico){
        monto = paqueteTuristico.getPrecioDePaquete();
        fechaDeVenta = LocalDate.now();
    }
}
