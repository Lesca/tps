package ar.edu.unlu.poo2023.modelo;

public abstract class Proovedor {
    private String razonSocial;
    private String telefono;
    private double precio;

    public Proovedor(String razonSocial, String telefono, double precio){
        setRazonSocial(razonSocial);
        setTelefono(telefono);
        setPrecio(precio);
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
}
