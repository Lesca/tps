package ar.edu.unlu.poo2023.modelo;

public class ProovedorDeHospedaje extends Proovedor {
    private TipoDeHospedaje tipoDeHospedaje;

    public ProovedorDeHospedaje(String razonSocial, String telefono, double precio, TipoDeHospedaje tipoDeHospedaje){
        super(razonSocial,telefono,precio);
        setTipoDeHospedaje(tipoDeHospedaje);
    }

    public TipoDeHospedaje getTipoDeHospedaje() {
        return tipoDeHospedaje;
    }

    public void setTipoDeHospedaje(TipoDeHospedaje tipoDeHospedaje) {
        this.tipoDeHospedaje = tipoDeHospedaje;
    }
}
