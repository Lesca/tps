package ar.edu.unlu.poo2023.modelo;

public class ProovedorDeTransporte extends Proovedor{
    TipoDeTransporte tipoDeTransporte;

    public ProovedorDeTransporte(String razonSocial, String telefono, double precio, TipoDeTransporte tipoDeTransporte){
        super(razonSocial, telefono, precio);
        this.tipoDeTransporte = tipoDeTransporte;
    }

    public TipoDeTransporte getTipoDeTransporte() {
        return tipoDeTransporte;
    }

    public void setTipoDeTransporte(TipoDeTransporte tipoDeTransporte) {
        this.tipoDeTransporte = tipoDeTransporte;
    }
}
