package ar.edu.unlu.poo2023.modelo;

import java.util.ArrayList;
import java.util.List;

public class Destino {
    private String nombreDeDestino;
    private List<ProovedorDeTransporte> proovedoresDeTransportes;
    private List<ProovedorDeHospedaje> proovedoresDeHospedaje;
    private List<ProovedorDeExcursion> proovedoresDeExcursion;

    public Destino(String nombreDeDestino){
       setNombreDeDestino(nombreDeDestino);
       proovedoresDeExcursion = new ArrayList<>();
       proovedoresDeTransportes = new ArrayList<>();
       proovedoresDeHospedaje = new ArrayList<>();
    }

    public String getNombreDeDestino() {
        return nombreDeDestino;
    }

    public void setNombreDeDestino(String nombreDeDestino) {
        this.nombreDeDestino = nombreDeDestino;
    }

    public ProovedorDeTransporte buscarProovedorDeTransporte(String razonSocial){
        ProovedorDeTransporte proovedor = null;
        int i = 0;
        boolean encontre = false;
        while (!encontre && i <= proovedoresDeTransportes.size() - 1){
            proovedor = proovedoresDeTransportes.get(i);
            if (proovedor.getRazonSocial().equals(razonSocial)){
                encontre = true;
            }else {
                i++;
            }
        }
        if (!encontre){
            proovedor = null;
        }
        return proovedor;
    }

    public ProovedorDeHospedaje buscarProovedorDeHospedaje(String razonSocial){
        ProovedorDeHospedaje proovedor = null;
        int i = 0;
        boolean encontre = false;
        while (!encontre && i <= proovedoresDeHospedaje.size() - 1){
            proovedor = proovedoresDeHospedaje.get(i);
            if (proovedor.getRazonSocial().equals(razonSocial)){
                encontre = true;
            }else {
                i++;
            }
        }
        if (!encontre){
            proovedor = null;
        }
        return proovedor;
    }

    public ProovedorDeExcursion buscarProovedorDeExcursion(String razonSocial){
        ProovedorDeExcursion proovedor = null;
        int i = 0;
        boolean encontre = false;
        while (!encontre && i <= proovedoresDeExcursion.size() - 1){
            proovedor = proovedoresDeExcursion.get(i);
            if (proovedor.getRazonSocial().equals(razonSocial)){
                encontre = true;
            }else {
                i++;
            }
        }
        if (!encontre){
            proovedor = null;
        }
        return proovedor;
    }

    public boolean agregarProovedorDeTransporte(ProovedorDeTransporte proovedorDeTransporte){
        boolean operacion = false;
        if (buscarProovedorDeTransporte(proovedorDeTransporte.getRazonSocial()) == null){
            operacion = true;
            proovedoresDeTransportes.add(proovedorDeTransporte);
        }
        return operacion;
    }

    public boolean agregarProovedorDeHospedaje(ProovedorDeHospedaje proovedorDeHospedaje){
        boolean operacion = false;
        if (buscarProovedorDeHospedaje(proovedorDeHospedaje.getRazonSocial()) == null){
            operacion = true;
            proovedoresDeHospedaje.add(proovedorDeHospedaje);
        }
        return operacion;
    }

    public boolean agregarProovedorDeExcursion(ProovedorDeExcursion proovedorDeExcursion){
        boolean operacion = false;
        if (buscarProovedorDeExcursion(proovedorDeExcursion.getRazonSocial()) == null){
            operacion = true;
            proovedoresDeExcursion.add(proovedorDeExcursion);
        }
        return operacion;
    }

    public boolean eliminarProovedorDeTransporte(String razonSocial){
        boolean operacion = false;
        ProovedorDeTransporte proovedorDeTransporte = buscarProovedorDeTransporte(razonSocial);
        if (proovedorDeTransporte != null){
            operacion = true;
            proovedoresDeTransportes.remove(proovedorDeTransporte);
        }
        return operacion;
    }

    public boolean eliminarProovedorDeHospedaje(String razonSocial){
        boolean operacion = false;
        ProovedorDeHospedaje proovedorDeHospedaje = buscarProovedorDeHospedaje(razonSocial);
        if (proovedorDeHospedaje != null){
            operacion = true;
            proovedoresDeHospedaje.remove(proovedorDeHospedaje);
        }
        return operacion;
    }

    public boolean eliminarProovedorDeExcursion(String razonSocial){
        boolean operacion = false;
        ProovedorDeExcursion proovedorDeExcursion = buscarProovedorDeExcursion(razonSocial);
        if (proovedorDeExcursion != null){
            operacion = true;
            proovedoresDeExcursion.remove(proovedorDeExcursion);
        }
        return operacion;
    }

    public PaqueteTuristico armarPaqueteTuristico(String descripcion, String razonSocialTransporte, String razonSocialHospedaje,
                                                  String razonSocialExcursion){
        ProovedorDeTransporte proovedorDeTransporte = buscarProovedorDeTransporte(razonSocialTransporte);
        ProovedorDeHospedaje proovedorDeHospedaje = buscarProovedorDeHospedaje(razonSocialHospedaje);
        ProovedorDeExcursion proovedorDeExcursion = buscarProovedorDeExcursion(razonSocialExcursion);
        return PaqueteTuristico.armarPaquete(descripcion, this, proovedorDeTransporte, proovedorDeHospedaje, proovedorDeExcursion);
    }
}
