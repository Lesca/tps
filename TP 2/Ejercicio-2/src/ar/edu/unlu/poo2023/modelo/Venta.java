package ar.edu.unlu.poo2023.modelo;

import java.time.LocalDate;

public class Venta {
    private Cliente cliente;
    private PaqueteTuristico paqueteTuristico;
    private LocalDate fechaDeVenta;
    private double monto;

    public Venta(Cliente cliente, PaqueteTuristico paqueteTuristico){
        this.cliente = cliente;
        this.paqueteTuristico = paqueteTuristico;
        fechaDeVenta = LocalDate.now();
        monto = paqueteTuristico.getPrecioDePaquete();
    }

    public Cliente getCliente() {
        return cliente;
    }

    public PaqueteTuristico getPaqueteTuristico() {
        return paqueteTuristico;
    }

    public LocalDate getFechaDeVenta() {
        return fechaDeVenta;
    }

    public double getMonto() {
        return monto;
    }
}
