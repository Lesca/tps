package ar.edu.unlu.poo2023.modelo;

import java.util.ArrayList;
import java.util.List;

public class PaqueteTuristico {
    private String descripcion;
    private Destino destino;
    private ProovedorDeTransporte proovedorDeTransporte;
    private ProovedorDeHospedaje proovedorDeHospedaje;
    private List<ProovedorDeExcursion> proovedoresDeExcursion;

    private PaqueteTuristico(String descripcion,Destino destino, ProovedorDeTransporte proovedorDeTransporte, ProovedorDeHospedaje proovedorDeHospedaje,
                             ProovedorDeExcursion proovedorDeExcursion){
        this.destino = destino;
        proovedoresDeExcursion = new ArrayList<>();
        this.proovedorDeTransporte = proovedorDeTransporte;
        this.proovedorDeHospedaje = proovedorDeHospedaje;
        proovedoresDeExcursion.add(proovedorDeExcursion);
        this.descripcion = descripcion;
    }

    public String getDescripcion(){
        return this.descripcion;
    }

    public Destino getDestino(){
        return this.destino;
    }

    public static PaqueteTuristico armarPaquete(String descripcion, Destino destino, ProovedorDeTransporte proovedorDeTransporte, ProovedorDeHospedaje proovedorDeHospedaje,
                             ProovedorDeExcursion proovedorDeExcursion){

        return new PaqueteTuristico(descripcion, destino, proovedorDeTransporte, proovedorDeHospedaje, proovedorDeExcursion);
    }

    public ProovedorDeTransporte getProovedorDeTransporte() {
        return proovedorDeTransporte;
    }

    public ProovedorDeHospedaje getProovedorDeHospedaje() {
        return proovedorDeHospedaje;
    }

    public void agregarExcursion(ProovedorDeExcursion proovedorDeExcursion){
        proovedoresDeExcursion.add(proovedorDeExcursion);
    }

    public String getDescripcionDePaquete(){
        String tipoT;
        String tipoH;
        switch (proovedorDeTransporte.getTipoDeTransporte()){
            case MARITIMO:
                tipoT = "Maritimo";
                break;
            case AEREO:
                tipoT = "Aereo";
                break;
            case TERRESTRE:
                tipoT = "Terrestre";
                break;
            default:
                tipoT = "";
        }
        switch (proovedorDeHospedaje.getTipoDeHospedaje()){
            case HOSTERIAS:
                tipoH = "Hosterias";
                break;
            case HOTELES:
                tipoH = "Holetes";
                break;
            case BUNGALOS:
                tipoH = "Bungalos";
                break;
            default:
                tipoH = "";
        }
        String descripcion = "Descripcion: "+getDescripcion()+ "\n";
        descripcion += "Destino: "+destino.getNombreDeDestino()+"\n";
        descripcion += "Razon social de transporte: "+proovedorDeTransporte.getRazonSocial()+"\n";
        descripcion += "Tipo de transporte: "+tipoT+"\n";
        descripcion += "Razon Social de Hospedaje: "+proovedorDeHospedaje.getRazonSocial()+"\n";
        descripcion += "Tipo de hospedaje: "+tipoH+"\n";
        descripcion += "Excursiones: ";
        for (ProovedorDeExcursion ex: proovedoresDeExcursion){
            descripcion += ex.getRazonSocial()+" ";
        }
        return descripcion;
    }

    public double getPrecioDePaquete(){
        double precio = 0.0;
        precio = proovedorDeTransporte.getPrecio() + proovedorDeHospedaje.getPrecio();
        for (ProovedorDeExcursion pe: proovedoresDeExcursion){
            precio += pe.getPrecio();
        }
        precio = precio * 1.12;
        return precio;
    }
}
