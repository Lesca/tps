package ar.edu.unlu.poo2023.modelo;

public class ProovedorDeExcursion extends Proovedor {
    String nombreDeExcursion;
    int minutosDeExcursion;

    public ProovedorDeExcursion(String razonSocial, String telefono, double precio, String nombreDeExcursion,
                                int minutosDeExcursion){
        super(razonSocial, telefono, precio);
        setNombreDeExcursion(nombreDeExcursion);
        setMinutosDeExcursion(minutosDeExcursion);
    }

    public String getNombreDeExcursion() {
        return nombreDeExcursion;
    }

    public void setNombreDeExcursion(String nombreDeExcursion) {
        this.nombreDeExcursion = nombreDeExcursion;
    }

    public int getMinutosDeExcursion() {
        return minutosDeExcursion;
    }

    public void setMinutosDeExcursion(int minutosDeExcursion) {
        this.minutosDeExcursion = minutosDeExcursion;
    }
}
