import ar.edu.unlu.poo2023.modelo.*;

public class Test {
    public static void main(String[] args) {
        AgenciaDeTurismo agenciaDeTurismo = new AgenciaDeTurismo();
        Destino destino = new Destino("brasil");
        // Agrego proovedores al destino
        ProovedorDeExcursion proovedorDeExcursion = new ProovedorDeExcursion("Excursion1", "1111111",
                1000.0,"excursion a la selva",120);
        ProovedorDeTransporte proovedorDeTransporte = new ProovedorDeTransporte("Aerolinas", "222222",
                5000.0,TipoDeTransporte.AEREO);
        ProovedorDeHospedaje proovedorDeHospedaje = new ProovedorDeHospedaje("Hotel", "33333333",
                2000.0,TipoDeHospedaje.HOTELES);
        destino.agregarProovedorDeExcursion(proovedorDeExcursion);
        destino.agregarProovedorDeHospedaje(proovedorDeHospedaje);
        destino.agregarProovedorDeTransporte(proovedorDeTransporte);
        agenciaDeTurismo.agregarDestino(destino);
        proovedorDeExcursion = new ProovedorDeExcursion("Excursion2", "1111111",
                1000.0,"excursion a la basilica",30);
        proovedorDeTransporte = new ProovedorDeTransporte("COLECTIVO2", "222222",
                5000.0,TipoDeTransporte.TERRESTRE);
        proovedorDeHospedaje = new ProovedorDeHospedaje("Bungalo2", "33333333",
                2000.0,TipoDeHospedaje.BUNGALOS);
        destino.agregarProovedorDeExcursion(proovedorDeExcursion);
        destino.agregarProovedorDeHospedaje(proovedorDeHospedaje);
        destino.agregarProovedorDeTransporte(proovedorDeTransporte);
        agenciaDeTurismo.agregarDestino(destino);


        // agrego otro destino
        destino = new Destino("argentina");
        // Agrego proovedores al destino2
        proovedorDeExcursion = new ProovedorDeExcursion("Excursion1", "1111111",
                1000.0,"excursion a la selva",120);
        proovedorDeTransporte = new ProovedorDeTransporte("Aerolinas", "222222",
                5000.0,TipoDeTransporte.AEREO);
        proovedorDeHospedaje = new ProovedorDeHospedaje("Hotel", "33333333",
                2000.0,TipoDeHospedaje.HOTELES);
        destino.agregarProovedorDeExcursion(proovedorDeExcursion);
        destino.agregarProovedorDeHospedaje(proovedorDeHospedaje);
        destino.agregarProovedorDeTransporte(proovedorDeTransporte);
        agenciaDeTurismo.agregarDestino(destino);
        proovedorDeExcursion = new ProovedorDeExcursion("Excursion2", "1111111",
                1000.0,"excursion a la basilica",30);
        proovedorDeTransporte = new ProovedorDeTransporte("COLECTIVO2", "222222",
                5000.0,TipoDeTransporte.TERRESTRE);
        proovedorDeHospedaje = new ProovedorDeHospedaje("Bungalo2", "33333333",
                2000.0,TipoDeHospedaje.BUNGALOS);
        destino.agregarProovedorDeExcursion(proovedorDeExcursion);
        destino.agregarProovedorDeHospedaje(proovedorDeHospedaje);
        destino.agregarProovedorDeTransporte(proovedorDeTransporte);

        // armado de paquetes
        PaqueteTuristico paqueteTuristico = agenciaDeTurismo.armarPaqueteTuristico("Paquete 1",agenciaDeTurismo.buscarDestino("argentina"),
                "Aerolinas", "Hotel", "Excursion1");
        // prueba de pantalla
        System.out.println(paqueteTuristico.getDescripcionDePaquete());
    }
}