import ar.edu.unlu.poo2023.juegoPalabra.Diccionario;
import ar.edu.unlu.poo2023.juegoPalabra.JuegoDePalabras;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Diccionario diccionario = new Diccionario();
        diccionario.agregarPalabra("hola");
        diccionario.agregarPalabra("chau");
        diccionario.agregarPalabra("que");
        diccionario.agregarPalabra("como");
        diccionario.agregarPalabra("kiosco");
        diccionario.agregarPalabra("tostado");

        JuegoDePalabras juegoDePalabras = new JuegoDePalabras(diccionario);
        juegoDePalabras.agregarJugador("Diego");
        juegoDePalabras.agregarJugador("Juan");
        juegoDePalabras.agregarPalabra(juegoDePalabras.getJugador("Diego"), "Hola");
        juegoDePalabras.agregarPalabra(juegoDePalabras.getJugador("Diego"), "chau");
        juegoDePalabras.agregarPalabra(juegoDePalabras.getJugador("Diego"), "adios");
        juegoDePalabras.agregarPalabra(juegoDePalabras.getJugador("Juan"), "hola");
        juegoDePalabras.agregarPalabra(juegoDePalabras.getJugador("Juan"), "kiosco");
        juegoDePalabras.agregarPalabra(juegoDePalabras.getJugador("Juan"), "que");

        System.out.printf("El puntaje del Jugador Diego es: %d\n", juegoDePalabras.getJugador("Diego").getPuntaje());
        System.out.printf("El puntaje del Jugador Juan es: %d\n", juegoDePalabras.getJugador("Juan").getPuntaje());
    }
}