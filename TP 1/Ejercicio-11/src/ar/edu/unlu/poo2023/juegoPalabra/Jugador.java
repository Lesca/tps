package ar.edu.unlu.poo2023.juegoPalabra;

import java.util.ArrayList;

public class Jugador{
    private String nombre;
    private ArrayList<String> palabras;
    private int puntaje;

    public Jugador(String nombre) {
        palabras = new ArrayList<String>();
        setNombre(nombre);
        puntaje = 0;
    }

    public boolean agregarPalabra(String palabra, Diccionario diccionario){
        boolean validacion = diccionario.validarPalabra(palabra);
        if (diccionario.validarPalabra(palabra)){
            puntaje += calcularPuntaje(palabra);
            palabras.add(palabra);
        }
        return validacion;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public int calcularPuntaje(String palabra) {
        int puntaje = palabra.length();
        for (char letra : palabra.toCharArray()){
            if("kzxwqy".contains(String.valueOf(letra))){
                puntaje ++;
            }
        }
        return puntaje;
    }

    public int getPuntaje(){
        return puntaje;
    }
}
