package ar.edu.unlu.poo2023.juegoPalabra;

import java.util.Set;
import java.util.HashSet;

public class Diccionario {
    private Set<String> palabrasValidas;

    public Diccionario(){
        palabrasValidas = new HashSet<String>();
    }

    public boolean agregarPalabra(String palabra){
        boolean yaExiste = palabrasValidas.contains(palabra.toLowerCase());
        if (!yaExiste){
            palabrasValidas.add(palabra.toLowerCase());
        }
        return yaExiste;
    }

    public boolean validarPalabra(String palabra){
        return palabrasValidas.contains(palabra.toLowerCase());
    }
}
