package ar.edu.unlu.poo2023.juegoPalabra;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class JuegoDePalabras {
    private Map<String, Jugador> jugadores;
    private Diccionario diccionario;

    public JuegoDePalabras(Diccionario diccionario){
        this.diccionario = diccionario;
        jugadores = new HashMap<String, Jugador>();
    }

    public boolean agregarJugador(String nombre){
        boolean agregue;
        if (jugadores.containsKey(nombre)){
            agregue = false;
        } else {
            Jugador jugador = new Jugador(nombre);
            jugadores.put(jugador.getNombre(), jugador);
            agregue = true;
        }
        return agregue;
    }

    public Jugador getJugador(String nombre){
        return jugadores.get(nombre);
    }

    public boolean agregarPalabra(Jugador jugador, String palabra){
        return jugador.agregarPalabra(palabra, diccionario);
    }


}
