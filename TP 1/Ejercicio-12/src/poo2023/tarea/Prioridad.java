package poo2023.tarea;

public enum Prioridad {
    BAJA, MEDIA, ALTA, MUY_ALTA;
}
