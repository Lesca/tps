package poo2023.tarea;
import java.util.Comparator;

public class TareaComparator implements Comparator<Tarea> {
    @Override
    public int compare(Tarea tarea1, Tarea tarea2) {
        // Compara primero por prioridad
        int comparacionPorPrioridad = tarea2.getPrioridad().compareTo(tarea1.getPrioridad());

        // Si la comparación por prioridad es diferente de cero, devuelve el resultado
        if (comparacionPorPrioridad != 0) {
            return comparacionPorPrioridad;
        }

        // Si la comparación por prioridad es igual, compara por la fecha
        return tarea1.getFechaLimite().compareTo(tarea2.getFechaLimite());
    }
}
