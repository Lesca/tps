package poo2023.tarea;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class ListaDeTareas {
    private ArrayList<Tarea> tareas;

    public ListaDeTareas(){
        tareas = new ArrayList<Tarea>();
    }

    public void agregarNuevaTarea(Tarea tarea){
        tareas.add(tarea);
    }

    public Tarea buscarTareaPorDescripcion(String descripcion){
        Tarea tarea = null;
        boolean encontre = false;
        int i = 0;
        while (i < tareas.size() && !encontre){
            tarea = tareas.get(i);
            if (tarea.getDescripcion().equals(descripcion)){
                encontre = true;
            } else{
                i++;
            }
        }
        return tarea;
    }

    public void completarTarea(String descripcion){
        Tarea t = buscarTareaPorDescripcion(descripcion);
        t.completarTarea();
    }

    public void completarTarea(Tarea tarea){
        tarea.completarTarea();
    }

    public List<Tarea> listaDeTareasNoVencidas(){
        ArrayList<Tarea> ldt = new ArrayList<Tarea>();
        for (Tarea t: tareas){
            if (!t.isVencida() && !t.isCompleta()){
                ldt.add(t);
            }
        }
        Collections.sort(ldt, new TareaComparator());
        return ldt;
    }

    public String listaDeTareasNoVencidasToString(){
        List<Tarea> lista = listaDeTareasNoVencidas();
        String s = "";
        for (Tarea t: lista){
            s += t.toString() + "\n" ;
        }
        return s;
    }
}
