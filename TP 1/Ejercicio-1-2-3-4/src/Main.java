import ar.edu.unlu.poo.lista.ListaEnlazadaDoble;
import ar.edu.unlu.poo.pila.Pila;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        ListaEnlazadaDoble lista = new ListaEnlazadaDoble();
        lista.agregar("1");
        lista.agregar("2");
        lista.agregar("3");
        lista.agregar("4");
        //lista.eliminar("2");
        System.out.println(lista.toString());
        System.out.println("--------------------------------");
        lista.insertar("5",4);
        System.out.println(lista.toString());
        System.out.println("--------------------------------");
        Pila pila = new Pila();
        pila.apilar(1);
        pila.apilar(2);
        pila.apilar(3);
        pila.apilar(4);
        System.out.println(pila.recuperar());
    }
}