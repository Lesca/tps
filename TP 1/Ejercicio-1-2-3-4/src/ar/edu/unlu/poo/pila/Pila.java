package ar.edu.unlu.poo.pila;

public class Pila {
    private NodoPila tope = null;

    public boolean esVacia(){
        return (tope == null);
    }
    public void apilar(Object dato){
        NodoPila nuevoNodoPila = new NodoPila();
        nuevoNodoPila.setDato(dato);
        nuevoNodoPila.setProximo(tope);
        tope = nuevoNodoPila;
    }

    public Object desapilar(){
        Object dato = null;
        if (!esVacia()) {
            dato = tope.getDato();
            tope = tope.getProximo();
        }
        return dato;
    }

    public Object recuperar(){
        Object dato = new Object();
        if (esVacia()){
            dato = null;
        } else{
            dato = tope.getDato();
        }
        return dato;
    }
}
