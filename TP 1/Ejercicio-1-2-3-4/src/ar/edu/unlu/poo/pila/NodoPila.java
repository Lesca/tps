package ar.edu.unlu.poo.pila;

public class NodoPila {
    private Object dato;
    private NodoPila proximo = null;

    public void setDato(Object dato){
        this.dato = dato;
    }

    public void setProximo(NodoPila nodoPila){
        proximo = nodoPila;
    }

    public Object getDato(){
        return this.dato;
    }

    public NodoPila getProximo(){
        return this.proximo;
    }
}
