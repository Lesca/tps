package ar.edu.unlu.poo.lista;

public class NodoLista {
    private Object dato;
    private NodoLista proximo = null;

    public void setDato(Object dato){
        this.dato = dato;
    }

    public void setProximo(NodoLista nodoLista){
        proximo = nodoLista;
    }

    public Object getDato(){
        return this.dato;
    }

    public NodoLista getProximo(){
        return this.proximo;
    }
}
