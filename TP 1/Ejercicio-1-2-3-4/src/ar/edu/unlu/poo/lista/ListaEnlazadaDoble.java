package ar.edu.unlu.poo.lista;

public class ListaEnlazadaDoble {
    private NodoListaDE primero = null;
    private NodoListaDE ultimo = null;
    private int q_items = 0;

    public void agregar(Object dato){
        NodoListaDE nuevoNodoLista = new NodoListaDE();
        nuevoNodoLista.setDato(dato);
        nuevoNodoLista.setProximo(null);
        if (esVacia()){
            nuevoNodoLista.setAnterior(null);
            primero = nuevoNodoLista;
            ultimo = primero;
        } else {
            ultimo.setProximo(nuevoNodoLista);
            nuevoNodoLista.setAnterior(ultimo);
            ultimo = nuevoNodoLista;
        }
        q_items ++;
    }

    public boolean esVacia(){
        return (primero == null);
    }

    public String toString(){
        String acumulador = "";
        NodoListaDE nodoListaAux = primero;
        while (nodoListaAux != null){
            acumulador += nodoListaAux.getDato() + "\n";
            nodoListaAux = nodoListaAux.getProximo();
        }
        return acumulador;
    }

    public void eliminar(Object dato){
        if (!esVacia()){
            boolean encontre = false;
            NodoListaDE nodoAux = primero;
            while (nodoAux != null && !encontre){
                if (nodoAux.getDato().equals(dato)){
                    encontre = true;
                } else {
                    nodoAux = nodoAux.getProximo();
                }
            }
            if (encontre) {
                if (nodoAux == primero){
                    if (primero == ultimo) {
                        primero = null;
                        ultimo = primero;
                    } else {
                        primero = primero.getProximo();
                        primero.setAnterior(null);
                    }
                } else {
                    if (nodoAux == ultimo){
                        ultimo = ultimo.getAnterior();
                        ultimo.setProximo(null);
                    } else {
                        nodoAux.getAnterior().setProximo(nodoAux.getProximo());
                        nodoAux.getProximo().setAnterior(nodoAux.getAnterior());
                    }
                }
            }
            nodoAux = null;
            q_items --;
        }
    }

    public Object recueprar(int index){
        Object dato = null;
        if (!esVacia()){
            if (index >= 1 && index <= q_items){
                NodoListaDE nodoListaAux = primero;
                for (int i = 1; i < index; i++){
                    nodoListaAux = nodoListaAux.getProximo();
                }
                dato = nodoListaAux.getDato();
            }
        }
        return dato;
    }

    public void insertar(Object dato, int index){
        if (index >= 1 && index <= q_items){
            NodoListaDE nuevoNodoLista = new NodoListaDE();
            nuevoNodoLista.setDato(dato);
            if (index == 1){
                nuevoNodoLista.setProximo(primero);
                primero = nuevoNodoLista;
            } else {
                NodoListaDE nodoListaAux = primero;
                for (int i = 1; i < index-1; i++){
                    nodoListaAux = nodoListaAux.getProximo();
                }
                nuevoNodoLista.setProximo(nodoListaAux.getProximo());
                nodoListaAux.setProximo(nuevoNodoLista);
            }
        }
    }

    public Integer size(){
        Integer tamano = 0;
        if (!esVacia()){
            NodoListaDE nodo = primero;
            while (nodo != null){
                tamano ++;
                nodo = nodo.getProximo();
            }
        }
        return tamano;
    }
}
