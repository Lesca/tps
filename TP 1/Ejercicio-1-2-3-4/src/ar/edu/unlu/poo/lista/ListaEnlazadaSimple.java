package ar.edu.unlu.poo.lista;

public class ListaEnlazadaSimple {
    private NodoLista primero = null;
    private Integer q_items = 0;

   public void agregar(Object dato){
        NodoLista nuevoNodoLista = new NodoLista();
        nuevoNodoLista.setDato(dato);
        nuevoNodoLista.setProximo(null);
        q_items ++;
        if (esVacia()) {
            primero = nuevoNodoLista;
        }else{
            NodoLista nodoListaAux = primero;
            while (nodoListaAux.getProximo() != null) {
                nodoListaAux = nodoListaAux.getProximo();
            }
            nodoListaAux.setProximo(nuevoNodoLista);
        }
   }

   public boolean esVacia(){
       return (primero == null);
   }

   public String toString(){
       String acumulador = "";
       NodoLista nodoListaAux = primero;
       while (nodoListaAux != null){
           acumulador += nodoListaAux.getDato() + "\n";
           nodoListaAux = nodoListaAux.getProximo();
       }
       return acumulador;
   }

   public void eliminar(Object dato){
       if (!esVacia()) {
           NodoLista nodoListaAux = primero;
           boolean encontre = false;
           if (q_items == 1){
               primero = null;
           } else {
               if (nodoListaAux.getDato() == dato){
                   primero = primero.getProximo();
               } else {
                   while (nodoListaAux.getProximo() != null && !encontre) {
                       if (nodoListaAux.getProximo().getDato() == dato) {
                           encontre = true;
                       } else {
                           nodoListaAux = nodoListaAux.getProximo();
                       }
                   }
                   if (encontre) {
                       nodoListaAux.setProximo(nodoListaAux.getProximo().getProximo());
                   }
               }
           }
       q_items --;
       }
   }

   public Object recuperar(int posicionOrdinal){
       Object dato = null;
       if (!esVacia()){
           if (posicionOrdinal >= 1 && posicionOrdinal <= q_items) {
               NodoLista nodoListaAux = primero;
               int i = 1;
               while (i < posicionOrdinal) {
                   i++;
                   nodoListaAux = nodoListaAux.getProximo();
               }
               dato = nodoListaAux.getDato();
           }
       }
       return dato;
   }

   public void insertar(Object dato, int posicionOrdinal){
       if (posicionOrdinal>=1 && posicionOrdinal<=q_items){
           NodoLista nuevoNodoLista = new NodoLista();
           nuevoNodoLista.setDato(dato);
           if (posicionOrdinal == 1) {
               nuevoNodoLista.setProximo(primero);
               primero = nuevoNodoLista;
           } else {
               NodoLista nodoListaAux = primero;
               for (int i = 1; i<posicionOrdinal-1; i++){
                   nodoListaAux = nodoListaAux.getProximo();
               }
               nuevoNodoLista.setProximo(nodoListaAux.getProximo());
               nodoListaAux.setProximo(nuevoNodoLista);
           }
       }
   }

    public Integer size(){
        Integer tamano = 0;
        if (!esVacia()){
            NodoLista nodo = primero;
            while (nodo != null){
                tamano ++;
                nodo = nodo.getProximo();
            }
        }
        return tamano;
    }
}
