package ar.edu.unlu.poo.lista;

public class NodoListaDE {
    private NodoListaDE proximo = null;
    private NodoListaDE anterior = null;
    private Object dato;

    public void setProximo(NodoListaDE proximo){
        this.proximo = proximo;
    }

    public void setAnterior(NodoListaDE anterior){
        this.anterior = anterior;
    }

    public NodoListaDE getProximo(){
        return proximo;
    }

    public NodoListaDE getAnterior(){
        return anterior;
    }

    public Object getDato(){
        return dato;
    }

    public void setDato(Object dato){
        this.dato = dato;
    }
}
