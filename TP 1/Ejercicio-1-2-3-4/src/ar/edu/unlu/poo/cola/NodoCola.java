package ar.edu.unlu.poo.cola;

public class NodoCola {
    private Object dato;
    private NodoCola proximo = null;

    public Object getDato() {
        return dato;
    }

    public void setDato(Object dato) {
        this.dato = dato;
    }

    public NodoCola getProximo() {
        return proximo;
    }

    public void setProximo(NodoCola proximo) {
        this.proximo = proximo;
    }
}
