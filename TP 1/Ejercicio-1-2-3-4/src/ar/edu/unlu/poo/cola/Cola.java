package ar.edu.unlu.poo.cola;

public class Cola {
    private NodoCola inicio = null;
    private NodoCola fin = null;

    private boolean esVacia(){
        return (inicio == null);
    }

    private void encolar(Object dato){
        NodoCola nuevoNodo = new NodoCola();
        nuevoNodo.setDato(dato);
        if (esVacia()){
            inicio = nuevoNodo;
            inicio = fin;
        } else {
            fin.setProximo(nuevoNodo);
            fin = nuevoNodo;
        }
    }

    private Object desencolar(){
        Object dato = null;
        if (!esVacia()){
            dato = inicio.getDato();
            inicio = inicio.getProximo();
        }
        return dato;
    }

    private Object recuperar(){
        Object dato = null;
        if (!esVacia()){
            dato = inicio.getDato();
        }
        return dato;
    }
}
