package ar.edu.unlu.poo2023.tarea;


import ar.edu.unlu.poo2023.fecha.OperacionesConFechas;

import java.util.HashSet;
import java.util.Set;
import java.time.LocalDate;

public class Tarea{
    private boolean completa;
    private LocalDate fechaLimite;
    private LocalDate fechaRecordatorio;
    private LocalDate fechaFinalizacion;
    private String descripcion;
    private Prioridad prioridad;
    private Set<Colaborador> colaboradores;

    public Tarea(String descripcion, LocalDate fechaLimite, Prioridad prioridad){
        modificarDescripcion(descripcion);
        cambiarPrioridad(prioridad);
        this.fechaLimite = fechaLimite;
        this.colaboradores = new HashSet<Colaborador>();
        completa = false;
    }

    public Tarea(String descripcion, LocalDate fechaLimite, Prioridad prioridad, boolean completa){
        this(descripcion, fechaLimite, prioridad);
        this.completa = completa;
    }

    public Tarea(String descripcion, LocalDate fechaLimite, LocalDate fechaRecordatorio, Prioridad prioridad){
        this(descripcion, fechaLimite, prioridad);
        this.fechaRecordatorio = fechaRecordatorio;
    }

    public Tarea(String descripcion, LocalDate fechaLimite, LocalDate fechaRecordatorio, Prioridad prioridad, boolean completa){
        this(descripcion, fechaLimite, fechaRecordatorio, prioridad);
        this.completa = completa;
    }

    public boolean isVencida() {
        boolean respuesta;
        if (completa){
            respuesta = false;
        } else {
            if (OperacionesConFechas.fechaEsMayor(fechaLimite, LocalDate.now()) ||
                    OperacionesConFechas.fechaEsIgual(fechaLimite, LocalDate.now())) {
                respuesta = false;
            } else {
                respuesta = true;
            }
        }
        return respuesta;
    }

    public boolean isCompleta() {
        return completa;
    }

    public boolean completarTarea(Colaborador colaborador){
        boolean existeColaborador = false;
        if (colaboradores.contains(colaborador)){
            existeColaborador = true;
            fechaFinalizacion = LocalDate.now();
            completa = true;
            colaborador.agregarTareaCompletada(this);
        }
        return existeColaborador;
    }

    public void cambiarPrioridad(Prioridad prioridad){
        this.prioridad = prioridad;
    }
    public void setFechaRecordatorio(LocalDate fechaRecordatorio){
        this.fechaRecordatorio = fechaRecordatorio;
    }

    public LocalDate getFechaRecordatorio(){
        return this.fechaRecordatorio;
    }

    public LocalDate getFechaLimite() {
        return fechaLimite;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void modificarDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean isPorVencer(){
        return (!completa && (OperacionesConFechas.fechaEstaEntre(LocalDate.now(),fechaRecordatorio,fechaLimite)));
    }

    public void elevarPrioridadSiPorVencer(){
        if (isPorVencer()){
            prioridad = Prioridad.MUY_ALTA;
        }
    }

    public Prioridad getPrioridad(){
        return this.prioridad;
    }

    public String toString() {
        String mensaje;
        mensaje = isVencida() ? "(Vencida) " : "";
        mensaje += isPorVencer() && !isVencida() ? "(Por vencer) ":"";
        mensaje += descripcion;
        return mensaje;
    }

    public void agregarColaborador(Colaborador colaborador){
        colaboradores.add(colaborador);
    }

    public LocalDate getFechaFinalizacion(){
        return this.fechaFinalizacion;
    }

    public Set<Colaborador> getColaboradores(){
        return this.colaboradores;
    }
}
