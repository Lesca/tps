package ar.edu.unlu.poo2023.tarea;

import java.util.ArrayList;
import java.util.List;

public class Colaborador {
    private String nombre;
    private List<Tarea> listaDeTareasCompletadas;

    public Colaborador(String nombre){
        this.nombre = nombre;
        this.listaDeTareasCompletadas = new ArrayList<Tarea>();
    }

    public String getNombre(){
        return this.nombre;
    }

    public void agregarTareaCompletada(Tarea tarea){
        listaDeTareasCompletadas.add(tarea);
    }

    public List<Tarea> getListaDeTareasCompletadas(){
        return this.listaDeTareasCompletadas;
    }
}
