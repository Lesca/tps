import ar.edu.unlu.poo2023.fecha.OperacionesConFechas;
import ar.edu.unlu.poo2023.tarea.Colaborador;
import ar.edu.unlu.poo2023.tarea.ListaDeTareas;
import ar.edu.unlu.poo2023.tarea.Prioridad;
import ar.edu.unlu.poo2023.tarea.Tarea;
import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
        LocalDate fecharMañana = OperacionesConFechas.obtenerFechaDesdeString("06-09-2023","dd-MM-yyyy");
        LocalDate fechaAyer = OperacionesConFechas.obtenerFechaDesdeString("05-09-2023","dd-MM-yyyy");
        LocalDate fechaRecordatorio = OperacionesConFechas.obtenerFechaDesdeString("06-09-2023","dd-MM-yyyy");
        Colaborador colaborador1 = new Colaborador("Diego");
        Colaborador colaborador2 = new Colaborador("Juan");
        Colaborador colaborador3 = new Colaborador("Pedro");
        Tarea tarea1 = new Tarea("Ir al supermercado mañana", fecharMañana, fechaRecordatorio, Prioridad.ALTA);
        Tarea tarea2 = new Tarea("Consultar respuesta del auto", fechaAyer, fechaRecordatorio, Prioridad.BAJA, true);
        Tarea tarea3 = new Tarea("Ir al cine a ver la nueva película de Marvel", fechaAyer, fechaRecordatorio, Prioridad.MEDIA);
        LocalDate fecha = OperacionesConFechas.obtenerFechaDesdeString("25-09-2023","dd-MM-yyyy");
        Tarea tarea4 = new Tarea("Tarea 4", fecha, fechaRecordatorio, Prioridad.ALTA);
        Tarea tarea5 = new Tarea("Tarea 5", fecha, fechaRecordatorio, Prioridad.MUY_ALTA);
        fecha = OperacionesConFechas.obtenerFechaDesdeString("10-09-2023","dd-MM-yyyy");
        fechaRecordatorio = OperacionesConFechas.obtenerFechaDesdeString("09-09-2023","dd-MM-yyyy");
        Tarea tarea6 = new Tarea("Tarea 6", fecha, fechaRecordatorio, Prioridad.MUY_ALTA);
        Tarea tarea7 = new Tarea("Tarea 7", fecha, fechaRecordatorio, Prioridad.BAJA);
        Tarea tarea8 = new Tarea("Tarea 8", fecha, fechaRecordatorio, Prioridad.ALTA);

        tarea1.agregarColaborador(colaborador1);
        tarea2.agregarColaborador(colaborador1);
        tarea3.agregarColaborador(colaborador1);
        tarea4.agregarColaborador(colaborador2);
        tarea5.agregarColaborador(colaborador2);
        tarea6.agregarColaborador(colaborador3);
        tarea7.agregarColaborador(colaborador3);
        tarea8.agregarColaborador(colaborador3);

        ListaDeTareas listaDeTareas = new ListaDeTareas();
        listaDeTareas.agregarNuevaTarea(tarea1);
        listaDeTareas.agregarNuevaTarea(tarea2);
        listaDeTareas.agregarNuevaTarea(tarea3);
        listaDeTareas.agregarNuevaTarea(tarea4);
        listaDeTareas.agregarNuevaTarea(tarea5);
        listaDeTareas.agregarNuevaTarea(tarea6);
        listaDeTareas.agregarNuevaTarea(tarea7);
        listaDeTareas.agregarNuevaTarea(tarea8);
        listaDeTareas.completarTarea("Tarea 4", colaborador2);
        listaDeTareas.completarTarea("Tarea 5", colaborador2);
        listaDeTareas.completarTarea("Ir al supermercado mañana", colaborador1);
        listaDeTareas.completarTarea("Tarea 7", colaborador3);

        System.out.println(listaDeTareas.listaDeTareasNoVencidasToString());
        System.out.println(listaDeTareas.listaDeTareasCompletadasPorColaborador(colaborador1).toString());
        System.out.println(listaDeTareas.listaDeTareasCompletadasPorColaborador(colaborador2).toString());
        System.out.println(listaDeTareas.listaDeTareasCompletadasPorColaborador(colaborador3).toString());
    }
}