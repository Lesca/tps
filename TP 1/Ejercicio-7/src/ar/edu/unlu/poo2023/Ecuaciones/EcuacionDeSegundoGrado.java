package ar.edu.unlu.poo2023.Ecuaciones;
public class EcuacionDeSegundoGrado {
    private double coeficienteCuadratico;
    private double coeficienteLineal;
    private double constante;

    public EcuacionDeSegundoGrado(double coeficienteCuadratico, double coeficienteLineal, double constante){
        this.coeficienteCuadratico = coeficienteCuadratico;
        this.coeficienteLineal = coeficienteLineal;
        this.constante = constante;
    }

    public double getCoeficienteCuadratico() {
        return coeficienteCuadratico;
    }

    public double getCoeficienteLineal() {
        return coeficienteLineal;
    }

    public double getConstante() {
        return constante;
    }

    public double calcularY(double x){
        return (coeficienteCuadratico*Math.pow(x,2)) + (coeficienteLineal*x) + constante;
    }

    public RaicesRealesDeEcuacionGrado2 resolverEcuacion(){
        RaicesRealesDeEcuacionGrado2 resultado;
        Double raiz1 = null;
        Double raiz2 = null;
        double a = coeficienteCuadratico;
        double b = coeficienteLineal;
        double c = constante;
        double discriminante = Math.pow(b,2)-(4*a*c);
        if (discriminante > 0){
            raiz1 = (-b+Math.sqrt(discriminante))/ 2*a;
            raiz2 = (-b-Math.sqrt(discriminante))/ 2*a;
        } else {
            if (discriminante == 0) {
                raiz1 = (-b) / (2 * a);
                raiz2 = raiz1;
            }
        }
        resultado = new RaicesRealesDeEcuacionGrado2(raiz1, raiz2);
        return resultado;
    }
}
