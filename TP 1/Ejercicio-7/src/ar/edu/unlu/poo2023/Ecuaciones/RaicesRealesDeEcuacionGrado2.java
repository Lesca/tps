package ar.edu.unlu.poo2023.Ecuaciones;

public class RaicesRealesDeEcuacionGrado2 {
    private Double raiz1;
    private Double raiz2;

    public RaicesRealesDeEcuacionGrado2(Double raiz1, Double raiz2){
        setRaiz1(raiz1);
        setRaiz2(raiz2);
    }

    public Double getRaiz1() {
        return raiz1;
    }

    public void setRaiz1(Double raiz1) {
        this.raiz1 = raiz1;
    }

    public Double getRaiz2() {
        return raiz2;
    }

    public void setRaiz2(Double raiz2) {
        this.raiz2 = raiz2;
    }

    public String toString(){
        String s = "";
        if (raiz1 != null){
            s = "Raiz 1: " + raiz1.toString()+ "\n";
            if (raiz2 != null){
                s += "Raiz 2: " + raiz2.toString();
            }
        }
        return s;
    }
}
