import ar.edu.unlu.poo2023.Ecuaciones.EcuacionDeSegundoGrado;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        EcuacionDeSegundoGrado ecu = new EcuacionDeSegundoGrado(1,-4,2);
        System.out.println(ecu.resolverEcuacion().toString());
        System.out.printf("f(%5.2f)= %5.2f",3.00,ecu.calcularY(3));
    }
}