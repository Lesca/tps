package ar.edu.unlu.poo2023.passwords;
import java.util.Random;
public class Password {
    private final char[] caracteres = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K',
    'L','M','N','Ñ','O','P','Q','R','S','T','U','V','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m',
    'n','ñ','o','p','q','r','s','t','u','v','x','y','z'};
    private String password = null;

    public Password(int longitud){
        generarPassword(longitud);
    }

    public Password(){
        this(8);
    }

    public String generarPassword(){
        return this.generarPassword(8);
    }

    public String generarPassword(int longitud){
        if (longitud>=8){
            String clave = "";
            Random r = new Random();
            for (int i = 0; i < longitud; i++){
                clave += caracteres[r.nextInt(61)];
            }
            this.password = clave;
        }
        return password;
    }

    public String generarPasswordFuerte(){
        generarPassword();
        while (!esFuerte()){
            generarPassword();
        }
        return this.password;
    }

    public String generarPasswordFuerte(int longitud){
        generarPassword(longitud);
        while (!esFuerte()){
            generarPassword(longitud);
        }
        return this.password;
    }

    public boolean esFuerte(){
        int contadorMayus = 0;
        int contadorNumeros = 0;
        int contadorminus = 0;
        int i = 0;
        boolean fuerte = false;
        for (char c : password.toCharArray()){
            if(c>=0 && c<=57){
                contadorNumeros ++;
            } else if (c>=65 && c<=90) {
                contadorMayus ++;
            } else if(c>=97 && c<=122){
                contadorminus ++;
            }
        }
        if (contadorMayus>=2 && contadorminus>=1 && contadorNumeros>=2){
            fuerte = true;
        }
        return fuerte;
    }

    public String getPassword(){
        return this.password;
    }
}
