import ar.edu.unlu.poo2023.passwords.Password;

import java.util.ArrayList;
import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Password pass;
        ArrayList<Password> passwords = new ArrayList<Password>();
        Scanner sc = new Scanner(System.in);
        String opcion;
        int tamaño;
        do {
            System.out.print("Ingrese el tamaño del password (no puede ser menor a 8 caracteres): ");
            tamaño = sc.nextInt();
            pass = new Password(tamaño);
            passwords.add(pass);
            System.out.printf("La contraseña es: %s <%s>\n",pass.getPassword(), pass.esFuerte() ? "Fuerte" : "Debil");
            sc.nextLine();
            System.out.print("Quiere seguir cargando Passwords (S/N)? ");
            opcion = sc.nextLine();
        } while (opcion.equals("S") || opcion.equals("s"));
    }
}