package ar.edu.unlu.poo2023.fecha;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class OperacionesConFechas{
    public static LocalDate obtenerFechaDesdeString(String fecha, String formato){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(formato);
        return LocalDate.parse(fecha, formatter);
    }

    public static boolean fechaEstaEntre(LocalDate fecha, LocalDate fechaInicio, LocalDate fechaFin){
        return (fecha.isEqual(fechaInicio) || fecha.isAfter(fechaInicio))
                && (fecha.isEqual(fechaFin) || fecha.isBefore(fechaFin));
    }

    public static boolean fechaEsMayor(LocalDate fecha1, LocalDate fecha2) {
        return fecha1.isAfter(fecha2);
    }

    public static boolean fechaEsMenor(LocalDate fecha1, LocalDate fecha2) {
        return fecha1.isBefore(fecha2);
    }

    public static boolean fechaEsIgual(LocalDate fecha1, LocalDate fecha2){
        return (fecha1.isEqual(fecha2));
    }
}
