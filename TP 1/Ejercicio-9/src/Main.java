import ar.edu.unlu.poo2023.fecha.OperacionesConFechas;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Main {
    public static void main(String[] args) {
        LocalDate fecha1 = OperacionesConFechas.obtenerFechaDesdeString("15-02-1993", "dd-MM-yyyy");
        LocalDate fecha2 = OperacionesConFechas.obtenerFechaDesdeString("15-05-1992", "dd-MM-yyyy");
        LocalDate fecha3 = OperacionesConFechas.obtenerFechaDesdeString("06-09-2023","dd-MM-yyyy");
        System.out.println(fecha1.toString());
        System.out.printf("la Fecha %s es %s que la fecha %s\n", fecha1.toString(), OperacionesConFechas.fechaEsMenor(fecha1,fecha2)? "Menor":"Mayor", fecha2.toString());
        System.out.printf("la Fecha %s %s entre las fechas %s, %s\n", fecha1.toString(), OperacionesConFechas.fechaEstaEntre(fecha1,fecha2,fecha3) ? "Esta":"No Esta",
                fecha2.toString(), fecha3.toString());
    }
}