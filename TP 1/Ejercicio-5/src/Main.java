import ar.edu.unlu.poo2023.tarea.Tarea;
import java.util.ArrayList;
import java.time.LocalDate;
import ar.edu.unlu.poo2023.tarea.*;

public class Main {
    public static void main(String[] args) {
        ArrayList<Tarea> listaDeTareas = new ArrayList<Tarea>();
        Tarea miTarea;
        LocalDate fecha;
        fecha = LocalDate.now().plusDays(1);
        listaDeTareas.add(miTarea = new Tarea("Ir al supermercado mañana",fecha,  Prioridad.ALTA));
        fecha = LocalDate.now().minusDays(1);
        listaDeTareas.add(miTarea = new Tarea("Consultar Repuesto del auto",fecha, Prioridad.ALTA, true));
        fecha = LocalDate.now().minusDays(1);
        listaDeTareas.add(miTarea = new Tarea("Ir al cine a ver la nueva pelitica de Marvel",fecha, Prioridad.ALTA));

        System.out.println(listaDeTareas.get(0).mostrarTarea());
        System.out.println(listaDeTareas.get(1).mostrarTarea());
        System.out.println(listaDeTareas.get(2).mostrarTarea());
    }
}