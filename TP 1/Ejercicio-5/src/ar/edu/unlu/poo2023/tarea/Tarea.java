package ar.edu.unlu.poo2023.tarea;
import java.time.LocalDate;

public class Tarea{
    private boolean completa;
    private LocalDate fechaLimite;
    private String descripcion;
    private Prioridad prioridad;

    public Tarea(String descripcion, LocalDate fechaLimite, Prioridad prioridad){
        modificarDescripcion(descripcion);
        cambiarPrioridad(prioridad);
        this.fechaLimite = fechaLimite;
        completa = false;
    }

    public Tarea(String descripcion, LocalDate fechaLimite, Prioridad prioridad, boolean completa){
        this(descripcion, fechaLimite, prioridad);
        this.completa = completa;
    }

    public boolean isVencida() {
        boolean respuesta;
        if (completa){
            respuesta = false;
        } else {
            if (fechaLimite.isAfter(LocalDate.now()) || fechaLimite.isEqual(LocalDate.now())) {
                respuesta = false;
            } else {
                respuesta = true;
            }
        }
        return respuesta;
    }

    public boolean isCompleta() {
        return completa;
    }

    public void completarTarea(){
        this.completa = true;
    }

    public void cambiarPrioridad(Prioridad prioridad){
        this.prioridad = prioridad;
    }

    public LocalDate getFechaLimite() {
        return fechaLimite;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void modificarDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String mostrarTarea() {
        String mensaje = isVencida() ? "(vencida) " : "";
        mensaje += descripcion;
        return mensaje;
    }
}
