import ar.edu.unlu.poo2023.fechas.Fecha;
import ar.edu.unlu.poo2023.tarea.Prioridad;
import ar.edu.unlu.poo2023.tarea.Tarea;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Fecha fecharMañana = new Fecha("07-09-2023", "dd-MM-yyyy");
        Fecha fechaAyer = new Fecha("05-09-2023", "dd-MM-yyyy");
        Fecha fechaRecordatorio = new Fecha("06-09-2023", "dd-MM-yyyy");

        Tarea tarea1 = new Tarea("Ir al supermercado mañana", fecharMañana, fechaRecordatorio, Prioridad.ALTA);
        Tarea tarea2 = new Tarea("Consultar respuesta del auto", fechaAyer, fechaRecordatorio, Prioridad.BAJA, true);
        Tarea tarea3 = new Tarea("Ir al cine a ver la nueva película de Marvel", fechaAyer, fechaRecordatorio, Prioridad.MEDIA);

        System.out.println("Tarea 1: " + tarea1.toString());
        System.out.println("Tarea 2: " + tarea2.toString());
        System.out.println("Tarea 3: " + tarea3.toString());
    }
}