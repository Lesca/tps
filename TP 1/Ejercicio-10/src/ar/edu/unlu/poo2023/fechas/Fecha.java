package ar.edu.unlu.poo2023.fechas;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Fecha {

    private LocalDate fecha;
    private DateTimeFormatter formato;

    public Fecha(String fecha, String formato){
        this.formato = DateTimeFormatter.ofPattern(formato);
        this.fecha = LocalDate.parse(fecha, this.formato);
    }

    public LocalDate getFecha(){
        return this.fecha;
    }

    public DateTimeFormatter getFormato(){
        return this.formato;
    }

    public String toString(){
        return this.fecha.format(formato);
    }

    public boolean estaEntreDosFechas(LocalDate fecha1, LocalDate fecha2){
        return (fecha.isEqual(fecha1) || fecha.isAfter(fecha1)) && (fecha.isEqual(fecha2) || fecha.isBefore(fecha2));
    }

    public boolean esMayor(LocalDate otraFecha){
        return fecha.isAfter(otraFecha);
    }

    public boolean esMenor(LocalDate otraFecha){
        return fecha.isBefore(otraFecha);
    }

    public boolean esIgual(LocalDate otraFecha){
        return fecha.isEqual(otraFecha);
    }
}
