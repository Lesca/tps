package ar.edu.unlu.poo2023.tarea;
import ar.edu.unlu.poo2023.fechas.Fecha;

import java.time.LocalDate;

public class Tarea{
    private boolean completa;
    private Fecha fechaLimite;
    private Fecha fechaRecordatorio;
    private String descripcion;
    private Prioridad prioridad;

    public Tarea(String descripcion, Fecha fechaLimite, Prioridad prioridad){
        modificarDescripcion(descripcion);
        cambiarPrioridad(prioridad);
        this.fechaLimite = fechaLimite;
        completa = false;
    }

    public Tarea(String descripcion, Fecha fechaLimite, Prioridad prioridad, boolean completa){
        this(descripcion, fechaLimite, prioridad);
        this.completa = completa;
    }

    public Tarea(String descripcion, Fecha fechaLimite, Fecha fechaRecordatorio, Prioridad prioridad){
        this(descripcion, fechaLimite, prioridad);
        this.fechaRecordatorio = fechaRecordatorio;
    }

    public Tarea(String descripcion, Fecha fechaLimite, Fecha fechaRecordatorio, Prioridad prioridad, boolean completa){
        this(descripcion, fechaLimite, fechaRecordatorio, prioridad);
        this.completa = completa;
    }

    public boolean isVencida() {
        boolean respuesta;
        if (completa){
            respuesta = false;
        } else {
            if (fechaLimite.esMayor(LocalDate.now()) || fechaLimite.esIgual(LocalDate.now())) {
                respuesta = false;
            } else {
                respuesta = true;
            }
        }
        return respuesta;
    }

    public boolean isCompleta() {
        return completa;
    }

    public void completarTarea(){
        this.completa = true;
    }

    public void cambiarPrioridad(Prioridad prioridad){
        this.prioridad = prioridad;
    }
    public void setFechaRecordatorio(Fecha fechaRecordatorio){
        this.fechaRecordatorio = fechaRecordatorio;
    }

    public Fecha getFechaRecordatorio(){
        return this.fechaRecordatorio;
    }

    public Fecha getFechaLimite() {
        return fechaLimite;
    }

    public String getDescripcion() {
        return descripcion;
    }
    public Prioridad getPrioridad(){
        return this.prioridad;
    }

    public void modificarDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean isPorVencer(){
        return (!completa && (fechaRecordatorio.esMenor(LocalDate.now()) || fechaRecordatorio.esIgual(LocalDate.now())));
    }

    public void elevarPrioridadSiPorVencer(){
        if (isPorVencer()){
            prioridad = Prioridad.MUY_ALTA;
        }
    }

    public String toString() {
        String mensaje;
        mensaje = isVencida() ? "(Vencida) " : "";
        mensaje += isPorVencer() && !isVencida() ? "(Por vencer) ":"";
        mensaje += descripcion;
        return mensaje;
    }
}
