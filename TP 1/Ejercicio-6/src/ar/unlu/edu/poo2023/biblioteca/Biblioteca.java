package ar.unlu.edu.poo2023.biblioteca;

import java.util.ArrayList;

public class Biblioteca {
    private ArrayList<Libro> libros;

    public Biblioteca(){
        libros = new ArrayList<>();
    }

    public void agregarNuevoLibro(String titulo, String autor, String año, int numeroPaginas, int numeroEjemplares){
        if (!buscarSiExiste(titulo, autor, año)){
            Libro nuevoLibro = new Libro(titulo, autor, año, numeroPaginas, numeroEjemplares);
            libros.add(nuevoLibro);
        }
    }

    public Libro buscarLibroPorTitulo(String titulo){
        Libro libro = null;
        int i = 0;
        boolean encontre = false;
        while (i <= libros.size() - 1 && !encontre){
            libro = libros.get(i);
            if (titulo.equals(libro.getTitulo())){
                encontre = true;
            } else {
                i ++;
            }
        }
        return libro;
    }

    public Libro buscarLibroPorAutor(String autor){
        Libro libro = null;
        int i = 0;
        boolean encontre = false;
        while (i <= libros.size() - 1 && !encontre){
            libro = libros.get(i);
            if (autor.equals(libro.getAutor())){
                encontre = true;
            } else {
                i ++;
            }
        }
        return libro;
    }

    public boolean buscarSiExiste(String titulo, String autor, String año){
        int i = 0;
        boolean encontre = false;
        Libro libro;
        while (i <= libros.size() - 1 && !encontre){
            libro = libros.get(i);
            if (titulo.equals(libro.getTitulo()) && autor.equals(libro.getAutor()) && año.equals(libro.getAño())){
                encontre = true;
            } else {
                i++;
            }
        }
        return encontre;
    }

    public boolean prestarLibro(Libro libro){
        return libro.prestar();
    }

    public boolean devolverLibro(Libro libro){
        return libro.devolver();
    }

    public Integer getCantidadDeLibrosPrestados(){
        Integer cantidad = 0;
        for (Libro l: libros){
            cantidad += l.getNumeroEjemplaresPrestados();
        }
        return cantidad;
    }
}
