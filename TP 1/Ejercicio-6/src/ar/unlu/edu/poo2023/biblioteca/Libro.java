package ar.unlu.edu.poo2023.biblioteca;

public class Libro {
    private String titulo;
    private String autor;
    private String isbn;
    private String año;
    private int numeroPaginas;
    private int numeroEjemplares;
    private int numeroEjemplaresPrestados;

    public Libro(String titulo, String autor, String año, int numeroPaginas, int numeroEjemplares){
        setTitulo(titulo);
        setAutor(autor);
        setNumeroPaginas(numeroPaginas);
        setNumeroEjemplares(numeroEjemplares);
        setAño(año);
        numeroEjemplaresPrestados = 0;
        this.isbn = "";
    }

    public Libro(String titulo, String autor, String año, String isbn, int numeroPaginas, int numeroEjemplares){
        this(titulo, autor, año, numeroPaginas, numeroEjemplares);
        this.isbn = isbn;
    }

    public Libro(String titulo, String autor, String año, int numeroPaginas, int numeroEjemplares,
    int numeroEjemplaresPrestados){
        this(titulo, autor, año, numeroPaginas, numeroEjemplares);
        this.numeroEjemplaresPrestados = numeroEjemplaresPrestados;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getAño() {
        return año;
    }

    public void setAño(String año) {
        this.año = año;
    }

    public int getNumeroPaginas() {
        return numeroPaginas;
    }

    public void setNumeroPaginas(int numeroPaginas) {
        if (numeroPaginas > 0){
            this.numeroPaginas = numeroPaginas;
        }
    }

    public int getNumeroEjemplares() {
        return numeroEjemplares;
    }

    private void setNumeroEjemplares(int numeroEjemplares) {
        if (numeroEjemplares >= 0){
            this.numeroEjemplares = numeroEjemplares;
        }
    }

    public int getNumeroEjemplaresPrestados() {
        return numeroEjemplaresPrestados;
    }

    public void setIsbn(String isbn){
        this.isbn = isbn;
    }

    public String getIsbn(){
        return isbn;
    }

    public boolean prestar(){
        boolean resultado = false;
        if (numeroEjemplares > 1) {
            numeroEjemplaresPrestados ++;
            numeroEjemplares --;
            resultado = true;
        }
        return resultado;
    }

    public boolean devolver(){
        boolean resultado = false;
        if (numeroEjemplaresPrestados > 0){
            numeroEjemplares ++;
            numeroEjemplaresPrestados --;
            resultado = true;
        }
        return resultado;
    }

    public void añadirEjemplares(int cantidad){
        numeroEjemplares += cantidad;
    }

    public String toString(){
        String s = "";
        s += "El libro <" + getTitulo() + "> ";
        s += "creado por el autor <" + getAutor() + "> ";
        s += "tiene <" + getNumeroPaginas() + "> paginas, ";
        s += "quedan <" + getNumeroEjemplares() + "> disponibles ";
        s += "y se prestaron <" + getNumeroEjemplaresPrestados() + ">";
        return s;
    }
}
