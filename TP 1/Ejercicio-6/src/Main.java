import ar.unlu.edu.poo2023.biblioteca.Biblioteca;
import ar.unlu.edu.poo2023.biblioteca.Libro;
public class Main {
    public static void main(String[] args) {
        Biblioteca biblioteca = new Biblioteca();
        biblioteca.agregarNuevoLibro("libro1","pepe", "2022",30,10);
        biblioteca.agregarNuevoLibro("libro2","jose", "2022",25,1);
        biblioteca.agregarNuevoLibro("libro3","carlos", "2022",50,50);
        System.out.println("--------------------------------------------------------------");
        System.out.println(biblioteca.buscarLibroPorTitulo("libro1").toString());
        System.out.println(biblioteca.buscarLibroPorTitulo("libro3").toString());
        biblioteca.prestarLibro(biblioteca.buscarLibroPorTitulo("libro1"));
        biblioteca.prestarLibro(biblioteca.buscarLibroPorTitulo("libro2"));
        biblioteca.prestarLibro(biblioteca.buscarLibroPorTitulo("libro3"));
        System.out.println("--------------------------------------------------------------");
        System.out.println("Cantidad de libros prestados: "+biblioteca.getCantidadDeLibrosPrestados());
        System.out.println("--------------------------------------------------------------");
        System.out.println(biblioteca.buscarLibroPorTitulo("libro1").toString());
        System.out.println(biblioteca.buscarLibroPorTitulo("libro2").toString());
        System.out.println(biblioteca.buscarLibroPorTitulo("libro3").toString());
        System.out.println("--------------------------------------------------------------");
        Libro l1 = biblioteca.buscarLibroPorTitulo("libro1");
        Libro l2 = biblioteca.buscarLibroPorTitulo("libro2");
        if (l1.getNumeroPaginas() > l2.getNumeroPaginas()){
            System.out.println("El libro <"+l1.getTitulo()+"> tiene mas paginas");
        } else {
            System.out.println("El libro <"+l2.getTitulo()+"> tiene mas paginas");
        }
    }
}